import processing.serial.*;
Serial port;

PFont abel;
PShape ship;

color green = color(142,255,20);
color grey = color(191);
color brown = color(76,54,41);
color blue = color(72, 147, 204);
color white = color(255);
color black = color(0);
color red = color(238,0,0);
color lightBlue = color(40,245,255);
color darkGrey = color(70);

int thumbstickX = 0;
int thumbstickY = 0;
int pitch = 0;
int roll = 0;
int heading = 0;
	
void setup() {

	size(800, 1280);
	//pixelDensity(displayDensity());
	frameRate(30);

	//port = new Serial(this, "/dev/tty.usbmodem1421",9600);
	//port = new Serial(this, "/dev/tty.usbmodem1411",9600);
	port = new Serial(this,"/dev/tty.Bluetooth-Incoming-Port",9600);
	//port = new Serial(this,"/dev/ttyACM0",9600);

	abel = loadFont("Abel-Regular-55.vlw");
	ship = loadShape("OrtegaShip.svg");
	rectMode(CORNER);

}

void draw() {

	background(0);
	noStroke();
	
	//Navigator
	pushMatrix();
	translate(width/2, 100);
	navigator(-pitch, roll);
	popMatrix();
	
	//Pitch
	tfPitchRoll("Pitch",pitch);

	//Roll
	pushMatrix();
	translate(width-200,0);
	tfPitchRoll("Roll",(roll/10));
	popMatrix();

	//Horizontal line
	stroke(grey);
	strokeWeight(2);
	line(0, 220, 800, 220);

	//Draw black bg to mask everything
	mask(221);

	//Motorinformation
	pushMatrix();
	translate(155, 1060);
	motorInfo("Motor   L", 10.5,  second());
	popMatrix();

	pushMatrix();
	translate(485, 1060);
	motorInfo("Motor   R", 2, (second()+ 37));
	popMatrix();

	//BatteryInfo
	pushMatrix();
	translate(45, 875);
	batteryInfo("Hancell 2", mouseX/8);
	popMatrix();

	pushMatrix();
	translate(45, 685);
	batteryInfo("Hancell 1", mouseX/8);
	popMatrix();

	pushMatrix();
	translate(595, 875);
	batteryInfo("Hancell 4", mouseX/8);
	popMatrix();

	pushMatrix();
	translate(595, 685);
	batteryInfo("Hancell 3", mouseX/8);
	popMatrix();

	//Thumbsticktargets
	pushMatrix();
	translate(125, 550);
	thumbstickTarget(green, ((thumbstickX - 499)/5) *-1, (thumbstickY - 512)/5, "Thrusters");
	popMatrix();
	
	pushMatrix();
	translate(675, 550);
	thumbstickTarget(lightBlue, ((thumbstickX- 488)/5) *-1,(thumbstickY - 512)/5, "Trim");
	popMatrix();

	//Motorbar triggers
	pushMatrix();
	translate(100, 1060);
	motorBar(thumbstickY, "L");
	popMatrix();

	pushMatrix();
	translate(660, 1060);
	motorBar(thumbstickY, "R");
	popMatrix();

	//Trimbars
	pushMatrix();
	translate(600, 420);
	trimbar(mouseX, "B");
	popMatrix();

	pushMatrix();
	translate(600, 645);
	trimbar(mouseX, "S");
	popMatrix();

	//thrusterbars
	pushMatrix();
	translate(45, 645);
	thrusterBar(-thumbstickX+1065);
	popMatrix();

	pushMatrix();
	translate(250, 470);
	rotate(0.5*PI);
	thrusterBar(thumbstickY+45);
	popMatrix();

	//draw the vectorship
	shape(ship, 325, 450, 150, 600);

	//Pressure information
	pushMatrix();
	translate(430, 515);
	pressureInfo((second()+ 37), lightBlue);
	popMatrix();

	pushMatrix();
	translate(430, 960);
	pressureInfo((second()+ 40), red);
	popMatrix();

	//Compass with wrap around
	pushMatrix();
	translate((heading*4.4), 240);
	compass();
	if(heading > 0){
		pushMatrix();
		translate(-1584,0);
		compass();
		popMatrix();
	}
	if(heading < 1580){
		pushMatrix();
		translate(1584,0);
		compass();
		popMatrix();	
	}
	popMatrix();

	//heading data
	pushMatrix();
	translate(0, 395);
	compassHeading(heading);
	popMatrix();

	// framerate
	surface.setTitle(int(frameRate) + " fps");
}

void tfPitchRoll (String grootheid, int numberToDisplay){
	//function that displays the pitch of the ortega
	//Displays: The angle in degrees 
	// 			Pitch or Roll 
	
	//draw a square with a black background
	fill(0);
	noStroke();
	rectMode(CORNER);
	rect(0, 0, 200, 220);
	
	//Use abel as font with size 55
	textFont(abel,55);
	fill(grey);
	textAlign(CENTER);

	//Draw the texts
	text(grootheid, 100, 80);
	fill(green);
	textFont(abel,70);

	String numberToDisplayString = nf(numberToDisplay,3,0);
	text(numberToDisplayString, 100, 158);
}

void navigator(int xoffset, float turningpoint){
	//function that draws the navigator
	// this translates the matrix according to the pitch and roll of the ortega
	//this draws the brown and blue bg first, with the perspective lines
	//then resets the matrix to the standard place
	//then draws the static indication lines.

	rectMode(CENTER);
	pushMatrix();
	noStroke();
	
	rotate(radians(turningpoint/10));
	translate(0, xoffset*2);
	
	fill(brown);
	rect(0,250, 800, 500);
	
	fill(blue);
	rect(0, -250, 800, 500); 

	//drawing perspective lines
	stroke(white);
	strokeWeight(4);
	//horizontal line
	line(-500,0,500,0);
	//perspective ground lines
		//inner
	line(-20, 0, -390, 500);
	line(20,0, 390, 500);
		//outer
	line(-50, 0, -960, 500);
	line(50, 0, 960, 500);
		//horizontal ground lines
	line(-15,20,15,20);
	line(-25,40,25,40);
	line(-35,60,35,60);
	
	stroke(brown);
	line(-15,-20,15,-20);
	line(-25,-40,25,-40);
	line(-35,-60,35,-60);

	popMatrix();

	//Part that doesnt move
	//dot
	stroke(red);
	fill(red);
	ellipseMode(CENTER);
	ellipse(0, 0, 8, 8);
	//stripes
	line(25, 0, 75, 0);
	line(-25,0,-75,0);
	//triangle
	strokeWeight(2);
	noFill();
	triangle(0, -87, -7, -70, 7, -70);
	//half circle
	strokeWeight(3);
	arc(0, 0, 50, 50, 0, PI/2);
}

void mask(int translatedownAmount){
	//Draws a mask that hides the not visible part of the artificial horizon
	rectMode(CORNER);
	noStroke();
	fill(black);

	pushMatrix();
	translate(0, translatedownAmount);
	rect(0, 0, width, height);
	popMatrix();
}

void thumbstickTarget( color targetColor, int xpos, int ypos, String textToDisplay){
	//Function that displays the input of the thumbsticks
	//Displays: The x and y position of said thumbstick
	// 			Whether its the thruster input or trim input (in text and color)

	//Text
	//Use abel as font with size 55
	textFont(abel,30);
	fill(grey);
	textAlign(CENTER);

	//Draw the texts
	text(textToDisplay, 0, -140);
	fill(green);
	
	ellipseMode(CENTER);
	noFill();
	//outer circle
	strokeWeight(2);
	stroke(white);
	ellipse(0, 0, 166, 166);

	//draw small grey circles using a for loop
	strokeWeight(1);
	stroke(grey);
	for (int i = 15; i < 150; i += 25) {
		
		ellipse(0, 0, i, i);
	}
	
	//small colored center dot
	fill(targetColor);
	noStroke();
	ellipse(0, 0, 5, 5);

	//moving dot
	pushMatrix();
	translate(xpos,ypos);
	fill(targetColor);
	noStroke();
	ellipse(0, 0, 22, 22);
	popMatrix();
}

void serialEvent (Serial port) {

	//function that takes care of the serial communication
	//Read all the data that comes in until you read an '*'
	//Seperate all numbers using an ','
	//Stick all the numbers in an array
	//Each position in the array holds specific data
	//When done reading, send a character, in this case an A
	String input = port.readStringUntil('*');
	if(input != null){
		println("Receiving: "+ input);
		//split it up
		int[] vals = int(splitTokens(input,",*"));
		thumbstickX= vals[0];
		thumbstickY = vals[1];
		heading = vals[2];
		pitch = vals[4];
		roll = vals[3];
	}
	port.write(65);
}

void keyPressed(){
	//press a key to send an capital A to start the transmission
	port.write(65);
}

void batteryInfo(String textToDisplay, int valueToDisplay){
	//function that displays information about the battery
	//Displays: which battery
	//          How much power is left (in %)
	//         	How much power is left in color fill of the bar

	//Changes color mode to HSB to change the color of the bar
	//Only the hue needs to be changed, the brightness and satuation stay the same
	colorMode(HSB,360,100,100);
	int hue = valueToDisplay;
	fill(hue,100,47);
	noStroke();
	strokeWeight(3);

	//Draw the colored square
	rect(0, 160, 160, -valueToDisplay*1.6);

	//draw the white border
	noFill();
	colorMode(RGB,255,255,255);
	stroke(white);
	rect(0, 0, 160, 160);
	
	//Draw the texts
	fill(grey);
	textAlign(CENTER);
	textFont(abel,35);
	text(textToDisplay, 80, 125);
	textFont(abel,40);
	text("%", 120, 75);
	fill(white);
	textFont(abel,60);
	text(valueToDisplay, 70, 75);
}

void motorInfo(String textToDisplay, float motorPower, int motorDegrees){
	//Function that displays information from the motor
	//Displays: which motor
	// 			How much power is being used currently (in kW and color and fill of the bar)
	//  		What the temprature is of the motor in deg celcuis

	colorMode(HSB,360,100,100);
	rectMode(CORNER);
	float hue = (motorPower*10.1);
	fill(hue,100,47);
	noStroke();
	rect(0, 0, 160, 200);

	colorMode(RGB,255,255,255);
	noFill();
	stroke(white);
	strokeWeight(3);
	rect(0, 0, 160, 200);

	pushMatrix();
	noStroke();
	fill(black);
	rectMode(CORNER);
	translate(0, - motorPower*18);
	rect(0, 0, 160, 200);
	popMatrix();

	noFill();
	stroke(white);
	strokeWeight(3);
	rect(0, 0, 160, 200);

	fill(grey);
	textAlign(CENTER);
	textFont(abel,35);
	text(textToDisplay, 80, 180);
	text("°C",100,145);
	textFont(abel, 30);
	text("Kw", 135, 75);

	textFont(abel,60);
	fill(lightBlue);

	text(motorDegrees, 45, 145);
	fill(white);
	String motorPowerString = nf(motorPower,2,1);
	text(motorPowerString, 60, 75);
}

void motorBar(int yTrigger, String barPlacement){
	//Function to display the input of the trigger thumbstick (backside of the steering-shaft)
	//Displays: how far the trigger is pushed in (in barsize and percentage)
	// 			Whether the trigger is pushed or pulled in red or green color

	//Draw a bar, adjust the barsize according to the trigger input
	//if the barinput is negative, make the bar green else make it red
	colorMode(RGB,255,255,255);
	strokeWeight(3);
	noStroke();
	rectMode(CORNER);

	float barSize = (yTrigger-510)/4.1;
	int percentage = (yTrigger-510)/4;
	
	if(barSize < 0){
		fill(green);
	}
	else{
		fill(red);
	}
	rect(0, 100, 40, barSize);
	
	stroke(white);
	noFill();
	rect(0, 0, 40, 200);
	line(0, 100, 40, 100);

	pushMatrix();
	translate(0, barSize);
	
	noStroke();
	fill(lightBlue);
	triangle(4, 100, -10, 95, -10, 105);
	triangle(36, 100, 50, 95, 50, 105);
	stroke(lightBlue);	
	line(0,100,40,100);
	
	if(barPlacement == "L"){

		stroke(lightBlue);
		noFill();
		
		rect(-87, 80, 74, 40);
		
		fill(green);
		textAlign(CENTER);
		textFont(abel,25);
		text(percentage, -55, 110);
		textFont(abel,20);
		text("%", -25,110);
	}

	if(barPlacement == "R"){

		stroke(lightBlue);
		noFill();
		
		rect(53, 80, 74, 40);
		
		fill(green);
		textAlign(CENTER);
		textFont(abel,25);
		text(percentage, 85, 110);
		textFont(abel,20);
		text("%", 115,110);
	}

	popMatrix();
}

void trimbar(int trimData, String barName){
	// Function that displays the information about trimming the boat
	//Displays: how much the trimtanks are filled
	// 			Whether its the bow or the stern tank
	// 			How much weight it corresponds to

	colorMode(RGB,255,255,255);
	int barSize = trimData;
	int kilogramdata = barSize;
	//build color
	rectMode(CORNER);
	noStroke();
	fill(lightBlue);
	rect(0, 0, barSize, 26);

	//black mask
	noStroke();
	fill(black);
	triangle(147, 0, 162, 0, 162, 13);
	triangle(147, 26, 162, 26, 162, 13);

	//build lines
	stroke(darkGrey);
	strokeWeight(2);
	for (int i = 0; i < 39; ++i) {
		
		line(i*4+4, 10,i*4+4 , 17);
	}
	
	stroke(white);	
	for (int i = 0; i < 9; ++i) {

	 	line(i *16+16, 6, i*16+16, 21);	
	}
	//build shape
	noFill();
	stroke(white);
	strokeWeight(3);
	beginShape();
	vertex(0, 0);
	vertex(145, 0);
	vertex(160, 13);
	vertex(145, 26);
	vertex(0, 26);
	endShape(CLOSE);

	//Font
	fill(grey);
	textAlign(CENTER);
	textFont(abel,30);
	text(barName, 180, 24);

	//Information block
	stroke(white);
	noFill();
	
	rect(-95, -6, 74, 40);
	line(-20, 12, -2, 12);
	fill(green);
	textAlign(CENTER);
	textFont(abel,25);
	text(kilogramdata, -70, 24);
	textFont(abel,20);
	text("kg", -40,24);
}

void thrusterBar(int stickData){
	//Function that displays the input from the steering thumbsticks
	//This serves like an alternative to the target based input information.
	//Diplays:	The x-value
	// 			The y-value

	colorMode(RGB,255,255,255);	
	
	//build color
	rectMode(CENTER);
	noStroke();
	fill(green);
	float stickDataflt = stickData / 7;
	rect(stickDataflt, 13, 40, 26);

	//build lines
	stroke(darkGrey);
	strokeWeight(2);
	for (int i = 0; i < 39; ++i) {
		
		line(i*4+4, 10,i*4+4 , 17);
	}
	
	stroke(white);	
	for (int i = 0; i < 9; ++i) {

	 	line(i *16+16, 6, i*16+16, 21);	
	}

	//build mask
	noStroke();
	fill(black);
	triangle(145, 0, 160, 0, 160, 13);
	triangle(145, 26, 160, 26, 160, 13);
	triangle(0, 0, 0, 13, 13, 0);
	triangle(0, 26, 0, 13, 13, 26);

	//Build border
	noFill();
	stroke(white);
	beginShape();
	vertex(0, 13);
	vertex(15, 0);
	vertex(145, 0);
	vertex(160, 13);
	vertex(145, 26);
	vertex(15, 26);
	endShape(CLOSE);

}

void compassHeading(int valueToDisplay){
	//Function that displays the angle of the boat relative to the north of the earth
	//Displays: the Angle in degrees( 	0/360 should be north)
	// 									90 should be east
	// 									180 should be south
	// 									270 should be west
	//Receives the heading data from a 9 dof board

	rectMode(CENTER);
	fill(black);
	stroke(lightBlue);
	strokeWeight(5);
	rect(400, 0, 130, 70);
	noFill();

	fill(green);
	textAlign(CENTER);
	textFont(abel,70);
	text(valueToDisplay, 400, 25);

	fill(lightBlue);
	line(396, -155, 396, -79);
	strokeWeight(1);
	triangle(396, -155, 402, -165, 390, -165);
	triangle(396, -79, 402, -69, 390, -69);
}

void pressureInfo(int valueToDisplay, color borderColor){
	//Function that displays the information from the pressure sensors
	//Has a color to differentiate which pressure sensor its displaying its data from

	//draw a rectangle with indicator color
	rectMode(CORNER);
	fill(black);
	stroke(borderColor);
	strokeWeight(5);
	rect(0, 0, 130, 70);
	noFill();
	line(0, 35, -35, 35);

	//draw the text to display data
	fill(green);
	textAlign(CENTER);
	textFont(abel,60);
	text(valueToDisplay, 45, 55);

	fill(grey);
	textAlign(CENTER);
	textFont(abel,30);
	text("m", 110, 55);

}

void compass() {
	//Function that displays your heading and functions as a compass
	//it draws a very long strip that is divided in a noth, east, south and west piece

	String displayheading = "N";
	stroke(255);
	strokeWeight(4);

	//horizontal lines
	line(0, 0, 1584, 0);
	line(0, 76, 1584, 76);
	
	//small grey lines
	for (int i = 0; i < 144; ++i) {
		stroke(darkGrey);
		line(i*11,26,i*11,50);
	}

	//big vertical lines
	for (int i = 0; i < 36; ++i) {
		stroke(255);
		line(i*44,13,i*44,63);
	}
	//very big lines
	for (int i = 0; i < 4; ++i) {
		stroke(255);
		line(i*396, 0, i*396, 76);

		//draw the  triangles
		noStroke();
		if(i == 0){
			fill(white);
			displayheading = "E";
		}
		if(i == 1){
			fill(red);
			displayheading = "N";
		}
		if(i == 2){
			fill(white);
			displayheading = "W";
		}
		if(i == 3){
			fill(white);
			displayheading = "S";
		}
		triangle(i*396, 0, i*396+6, -10, i*396-6, -10);
		triangle(i*396, 76, i*396+6, 86, i*396-6, 86);

		textFont(abel,30);
		textAlign(CENTER);
		text(displayheading, i*396, 112);
	}
}