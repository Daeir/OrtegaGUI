int stickPin = A8;
int stickValue = 0;

void setup() {

	pinMode(stickPin, INPUT);
	Serial.begin(9600);

}

void loop() {

	stickValue = analogRead(stickPin);
	Serial.print("AcceloStick ");
	Serial.println(stickValue);
	delay(20);
}
