/*****[Combined Current Ardiuno Code]*****/
//This is the code that currently runs on the arduino
//It features:  -  Working thumbsticks
//              -  Working 9DOF board
//              -  Working communication to processing

#include <Wire.h>
#include <Adafruit_Sensor.h>
#include <Adafruit_BNO055.h>
#include <utility/imumaths.h>

Adafruit_BNO055 bno = Adafruit_BNO055(55);

//Pins for the thumbsticks
int xPin = A0;
int yPin = A1;

//Variables for the thumbstick data
int xValue = 0;
int yValue = 0;

int rollValue = 0;

void setup() {

  //Setting Analog ports
  pinMode(xPin, INPUT);
  pinMode(yPin, INPUT);

  //Serial connection for communication with Processing
  Serial.begin(9600);
  
  //Start the 9DOF sensor
  bno.begin();

  delay(1000);
  //Set this thing for extra precision
  bno.setExtCrystalUse(true);
}

void loop() {

  if(Serial.available() > 0){
    //When processing sends something get dof data
    //Get rid of the data that has been sent --> empty the buffer
    Serial.read();
    //get dof data
    sensors_event_t event;
    bno.getEvent(&event);
    //get sensordata and send it over serial seperated by a ','
    //End with an * so we know its the end of the data
    rollValue = event.orientation.y;
    
    xValue = analogRead(xPin);
    yValue = analogRead(yPin);
  
    Serial.print(xValue,DEC);
    Serial.print(",");
    Serial.print(yValue,DEC);
    Serial.print(",");
    Serial.print(event.orientation.x, 0);
    Serial.print(",");
    Serial.print((event.orientation.y*10), 0);
    Serial.print(",");
    Serial.print(event.orientation.z, 0);
    Serial.print("*"); 
  } 
}

