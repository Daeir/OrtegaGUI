#include <Wire.h>
#include <Adafruit_Sensor.h>
#include <Adafruit_BNO055.h>
#include <utility/imumaths.h>

Adafruit_BNO055 bno = Adafruit_BNO055(55);

int xDofValue = 0;
int yDofValue = 0;
int zDofValue = 0;

void setup() {

	Serial.begin(9600);
 	
 	//Start the sensor/ if the sensor was not started, throw an error
	if(!bno.begin()){

		//Serial.print("DOF not detected!");
		while(1);
	}
	//Serial.print("DOF Running");
	delay(1000);
	bno.setExtCrystalUse(true);
}

void loop() {

	if(Serial.available() > 0){
		//When processing sends something get dof data
		//Get rid of the data that has been sent
		Serial.read();
		//get dof data
		sensors_event_t event;
		bno.getEvent(&event);
		//get x data rounded at whole numbers
		
		Serial.print(event.orientation.x, 0);
		Serial.print(",");
		Serial.print(event.orientation.y, 0);
		Serial.print(",");
		Serial.print(event.orientation.z, 0);
		Serial.print("*");
	
	}	
}
