int lineWidth = 600;
int xpos = 0;

void setup() {

	size(512, 512);
	pixelDensity(displayDensity());
	frameRate(30);
	
}

void draw() {
	
	background(0);

	stroke(255);
	strokeWeight(4);

	lineDrawing();

	stroke(175);
	
	if(xpos < 0){
		pushMatrix();
		translate(lineWidth, 0);
		lineDrawing();
		popMatrix();
	}

}

void lineDrawing(){

	line(xpos,200,lineWidth+xpos,200);
}

void keyPressed(){

	if(keyCode == 39){
		xpos +=10;
	}
	if(keyCode == 37 ){
		xpos -=10;
	}
}