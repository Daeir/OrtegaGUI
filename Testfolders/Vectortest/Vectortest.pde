PShape ship;

void setup() {
	
	size(512, 512);
	frameRate(30);
	pixelDensity(displayDensity());
	ship = loadShape("OrtegaShip.svg");
}

void draw() {

	background(0);
	stroke(255);
	strokeWeight(2);

	shape(ship, 100, 100,200,800);

	
}