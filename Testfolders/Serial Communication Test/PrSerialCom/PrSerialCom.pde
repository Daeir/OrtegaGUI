import processing.serial.*;

int x = 0;
int y = 0;

Serial port;

void setup() {

	size(800,800);	
	port = new Serial(this, "/dev/tty.usbmodem1421", 9600);

	port.write(65);
}

void draw() {

	background(0);
	fill(255);
	translate(x, y);
	rect(0, 0, 100, 100);
}

void serialEvent (Serial port) {

	String input = port.readStringUntil('*');
	if(input != null){
		println("Receiving: "+ input);
		//split it up
		int[] vals = int(splitTokens(input,",*"));
		x = vals[0];
		y = vals[1];
	}
	port.write(65);
	//when done ask again
}
void keyPressed() {
	port.write(65);	
}