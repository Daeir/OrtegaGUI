int xPin = A0;
int yPin = A1;

int xValue = 0;
int yValue = 0;

void setup() {
	
	pinMode(xPin, INPUT); 
	pinMode(yPin, INPUT); 
	Serial.begin(9600);
}

void loop() {

	if(Serial.available() > 0){

		//when processing sends something
		Serial.read();

		xValue = analogRead(xPin);
		yValue = analogRead(yPin);

		Serial.print(xValue,DEC);
		Serial.print(",");
		Serial.print(yValue,DEC);
		Serial.print("*");
	}
}
