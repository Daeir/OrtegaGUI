color darkGrey = color(70);
color red = color(238,0,0);
int heading = 0;

PFont abel;

void setup() {
	
	size(800, 800);
	pixelDensity(displayDensity());
	frameRate(30);

	abel = loadFont("Abel-Regular-55.vlw");

}

void draw() {

	background(0);
	pushMatrix();
	translate(heading, 200);
	compass();
	if(heading > 0){
		pushMatrix();
		translate(-1584,300);
		compass();
		popMatrix();
	}
	if(heading < 1584){
		pushMatrix();
		translate(1584,200);
		compass();
		popMatrix();	
	}
	popMatrix();
}

void compass() {
	String displayheading = "E";
	stroke(255);
	strokeWeight(4);

	//horizontal lines
	line(0, 0, 1584, 0);
	line(0, 76, 1584, 76);
	
	//small grey lines
	for (int i = 0; i < 144; ++i) {
		stroke(darkGrey);
		line(i*11,26,i*11,50);
	}

	//big vertical lines
	for (int i = 0; i < 36; ++i) {
		stroke(255);
		line(i*44,13,i*44,63);
	}
	//very big lines
	for (int i = 0; i < 4; ++i) {
		stroke(255);
		line(i*396, 0, i*396, 76);

		//draw the  triangles
		noStroke();
		if(i == 0){
			fill(255);
			displayheading = "W";
		}
		if(i == 1){
			fill(red);
			displayheading = "N";
		}
		if(i == 2){
			fill(255);
			displayheading = "E";
		}
		if(i == 3){
			fill(255);
			displayheading = "S";
		}
		triangle(i*396, 0, i*396+6, -10, i*396-6, -10);
		triangle(i*396, 76, i*396+6, 86, i*396-6, 86);

		textFont(abel,30);
		textAlign(CENTER);
		text(displayheading, i*396, 112);


	}
}

void keyPressed() {
	if(keyCode == 39){
		heading += 20;
	}
	if(keyCode == 37){
		heading -= 20;
	}
}